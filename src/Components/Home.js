import React from 'react'

function Home() {
  return (
    <div className='home'>
      <div className='intro-box'>
        <h1>Hi, there !</h1>
        <br/>
        <h1>My name is Kumar Varun</h1>
        <br/>
        <h1>Designation: Software Developer</h1>
      </div>
    </div>
  )
}

export default Home