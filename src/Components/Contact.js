import React from 'react'

function Contact() {
    return (
        <div className='contact'>
            <div className='contact-form'>
                <form>
                    <label htmlFor='fname'>First Name:&nbsp;</label>
                    <input type="text" name="fname" required />
                    <br/>
                    <br/>
                    <label htmlFor='lname'>Last Name:&nbsp;&nbsp;</label>
                    <input type="text" name="lname" required />
                    <br/>
                    <br/>
                    <label htmlFor='mobile'>Contact No:&nbsp;</label>
                    <input type="text" name="lname" required />
                    <br/>
                    <br/>
                    <input type="submit" value="Submit"></input>
                </form>
            </div>
            <div class="map-container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2583.781377850135!2d77.61306604904392!3d12.933824135580371!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae151947c69de7%3A0xf2b320fefa7ffb8c!2sMountBlue%20Technologies%20Private%20Limited!5e0!3m2!1sen!2sin!4v1651459595326!5m2!1sen!2sin" width="400" height="300" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    )
}

export default Contact