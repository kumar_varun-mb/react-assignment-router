import React from 'react'

function About() {
  return (
    <div className='about'>
      <div className='intro-box'>
        <div className='profile-picture'>
          <img></img>
        </div>
        <br/>
        <p>I'm an aspiring Full Stack Developer.<br/>
         Currently getting trained in MERN Stack at MountBlue Technologies.</p>
      </div>
    </div>
  )
}

export default About;